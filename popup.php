<div class="page-overlay"></div>
<!-- Video Popup -->
<div class="card card-popup card-popup-video">
    <div class="d-flex align-items-center justify-content-between mb-3">
        <div>
            <h6 >Video Explanation</h6>
        </div>
        <div>
            <a href="javascript:;" class="popup-close">&times;</a>
        </div>
    </div>
    <div class="card-popup-body">
        <div class="w-100">
            <div class="video-box">
                <img src="/ap-guru-html/dest/images/resourses/video.jpg" alt="" class="img-fluid">
                <img src="/ap-guru-html/dest/images/icons/video-play.svg" alt="" class="video-play">
            </div>
        </div> 
    </div>
</div>
    