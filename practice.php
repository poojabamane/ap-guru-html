<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include 'head.php';
        ?>
        <title>Practice | AP Guru</title>
        <meta name="description" content="">
    </head>
    <body class="active-page" id="journey-page">
    <?php
        include 'header.php';
        include 'sidebar.php';
    ?>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-2">
                <div class="card icon-card text-center">
                    <img src="<?= $baseurl; ?>dest/images/icons/journey/target.svg" alt="" class="mb-2">
                    <h3 class="card-score">66.6%</h3>
                    <p class="card-text">Average Accuracy</p>
                </div>
            </div>
            <div class="col-2">
                <div class="card icon-card text-center">
                    <img src="<?= $baseurl; ?>dest/images/icons/journey/active-time.svg" alt="" class="mb-2">
                    <h3 class="card-score text-orange">15s</h3>
                    <p class="card-text">Avg. Time Per Question</p>
                </div>
            </div>
            <div class="col-2">
                <div class="card icon-card text-center">
                    <img src="<?= $baseurl; ?>dest/images/icons/journey/digital-books.svg" alt="" class="mb-2">
                    <h3 class="card-score text-orange">20<span class="card-sub-score">/64</span></h3>
                    <p class="card-text">Quiz Completed</p>
                </div>
            </div>
            <div class="col-2">
                <a href="" class="card icon-card text-center bg-blue">
                    <img src="<?= $baseurl; ?>dest/images/icons/quiz.svg" alt="" class="mb-2 mt-2">
                    <div class="d-flex align-items-center quiz-text">
                        <p>Make Your Own Quiz</p>
                        <div class="">
                            <img src="<?= $baseurl; ?>dest/images/icons/next-white.svg" alt="">
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-12 d-flex">
                <div class="test-tab test-details-tab w-100">
                    <div class="tab-pane d-flex flex-wrap">
                        <div class="container-fluid no-padding">
                            <div class="row d-flex align-items-center">
                                <div class="col-12 d-flex">
                                    <div class="tab-title active-bg-blue">
                                        <h5>English</h5>
                                    </div>
                                    <div class="tab-title active active-bg-blue">
                                        <h5>Maths</h5>
                                    </div>
                                    <div class="tab-title active-bg-blue">
                                        <h5>Reading</h5>
                                    </div>
                                    <div class="tab-title active-bg-blue">
                                        <h5>Science</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-details mt-4">
                        <div class="row mt-4">
                            <div class="col-12">
                                <div class="card card-score journey">
                                    <div class="card-header">
                                        <h5>Recommended based on your perfomance</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="row d-flex align-items-center">
                                            <div class="col-10">
                                                <p class="NunitoSans-Bold mt-2 mb-2">Solving linear equations and linear inequalities</p>
                                                <p>Maths | 30 mins</p>
                                            </div>
                                            <div class="col-2">
                                                <button type="button" class="btn watch-btn">
                                                    <img src="<?= $baseurl; ?>dest/images/icons/play.svg" alt="" class="mr-2"> Start
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-6">
                                <div class="card card-box tests">
                                    <div class="card-header">
                                        <div class="d-flex">
                                            <h5>Pre Made Quizzes</h5>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="course-wrap practice-list w-100 scrollbar">
                                            <div class="topic-details-list d-flex align-items-center">
                                                <div class="col-8 no-padding">
                                                    <div class="d-flex">
                                                        <h6>Matrices</h6>
                                                        <span class="badge badge-weak ml-3">
                                                            <span class="badge-main">Weak</span>
                                                            <span class="badge-sub">12% Accuracy</span>
                                                        </span>
                                                    </div>
                                                    <p class="text-light-gray pt-2">Completed on 27 Dec 2019</p>
                                                </div>
                                                <div class="col-4">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" class="practice-control">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/re-test.svg" alt="">
                                                            <p class="hover-detils text-green">Re-test</p>
                                                        </a>
                                                        <a href="#" class="practice-control">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/report.svg" alt="">
                                                            <p class="hover-detils text-blue">Report</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="topic-details-list d-flex align-items-center">
                                                <div class="col-8 no-padding">
                                                    <div class="d-flex">
                                                        <h6>Formula Sheet</h6>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" class="practice-control text-quiz">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/green-next.svg" alt="">
                                                            <p class="hover-detils text-green">Take Quiz</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="topic-details-list d-flex align-items-center">
                                                <div class="col-8 no-padding">
                                                    <div class="d-flex">
                                                        <h6>Cyclic Number</h6>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" class="practice-control text-quiz">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/green-next.svg" alt="">
                                                            <p class="hover-detils  text-green">Take Quiz</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="topic-details-list d-flex align-items-center">
                                                <div class="col-8 no-padding">
                                                    <div class="d-flex">
                                                        <h6>Number Theory</h6>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" class="practice-control text-quiz">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/green-next.svg" alt="">
                                                            <p class="hover-detils  text-green">Take Quiz</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="topic-details-list d-flex align-items-center">
                                                <div class="col-8 no-padding">
                                                    <div class="d-flex">
                                                        <h6>Relations And Functions</h6>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" class="practice-control text-quiz">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/green-next.svg" alt="">
                                                            <p class="hover-detils  text-green">Take Quiz</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="topic-details-list d-flex align-items-center">
                                                <div class="col-8 no-padding">
                                                    <div class="d-flex">
                                                        <h6>Matrices</h6>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" class="practice-control text-quiz">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/green-next.svg" alt="">
                                                            <p class="hover-detils  text-green">Take Quiz</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="card card-box tests">
                                    <div class="card-header">
                                        <div class="d-flex">
                                            <h5>Quizzes Made By You</h5>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                    <div class="course-wrap practice-list w-100 scrollbar">
                                            <div class="topic-details-list d-flex align-items-center">
                                                <div class="col-8 no-padding">
                                                    <div class="d-flex">
                                                        <h6>Matrices</h6>
                                                        <span class="badge badge-weak ml-3">
                                                            <span class="badge-main">Weak</span>
                                                            <span class="badge-sub">12% Accuracy</span>
                                                        </span>
                                                    </div>
                                                    <p class="text-light-gray pt-2">Completed on 27 Dec 2019</p>
                                                </div>
                                                <div class="col-4">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" class="practice-control">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/re-test.svg" alt="">
                                                            <p class="hover-detils text-green">Re-test</p>
                                                        </a>
                                                        <a href="#" class="practice-control">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/report.svg" alt="">
                                                            <p class="hover-detils text-blue">Report</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="topic-details-list d-flex align-items-center">
                                                <div class="col-8 no-padding">
                                                    <div class="d-flex">
                                                        <h6>Formula Sheet</h6>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" class="practice-control text-quiz">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/green-next.svg" alt="">
                                                            <p class="hover-detils text-green">Take Quiz</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="topic-details-list d-flex align-items-center">
                                                <div class="col-8 no-padding">
                                                    <div class="d-flex">
                                                        <h6>Cyclic Number</h6>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" class="practice-control text-quiz">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/green-next.svg" alt="">
                                                            <p class="hover-detils  text-green">Take Quiz</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="topic-details-list d-flex align-items-center">
                                                <div class="col-8 no-padding">
                                                    <div class="d-flex">
                                                        <h6>Number Theory</h6>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" class="practice-control text-quiz">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/green-next.svg" alt="">
                                                            <p class="hover-detils  text-green">Take Quiz</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="topic-details-list d-flex align-items-center">
                                                <div class="col-8 no-padding">
                                                    <div class="d-flex">
                                                        <h6>Relations And Functions</h6>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" class="practice-control text-quiz">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/green-next.svg" alt="">
                                                            <p class="hover-detils  text-green">Take Quiz</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="topic-details-list d-flex align-items-center">
                                                <div class="col-8 no-padding">
                                                    <div class="d-flex">
                                                        <h6>Matrices</h6>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" class="practice-control text-quiz">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/green-next.svg" alt="">
                                                            <p class="hover-detils  text-green">Take Quiz</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        
    </div>
    <?php
        include 'footer.php';
    ?>
    </body>
</html>