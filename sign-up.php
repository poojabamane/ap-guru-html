<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'head.php';
    ?>
    <title>Sign Up | AP Guru</title>
    <meta name="description" content="">
    <link rel="stylesheet" type="text/css" href="<?= $baseurl; ?>dest/css/datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?= $baseurl; ?>dest/css/intlTelInput.css">
</head>

<body class="noheader">
    <?php
    include 'header.php';
    ?>
    <section class="sign-up-process1 d-none">
        <div class="vh-100 d-flex align-items-center">
            <div class="col-8 offset-2">
                <div class="card">
                    <div class="card-header text-center">
                        <img src="<?= $baseurl; ?>dest/images/logo.svg" alt="">
                    </div>
                    <div class="card-body">
                        <button class="col-6 offset-3 align-items-center btn google-btn">
                            <img src="<?= $baseurl; ?>dest/images/icons/google.svg" alt="">Continue with Google
                        </button>
                        <h4 class="text-center">Create Account</h4>
                        <form>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="signup-fullname">Full Name</label>
                                        <input type="text" class="form-control" id="signup-fullname">
                                    </div>
                                    <div class="form-group">
                                        <label for="signup-number">Your Mobile Number</label>
                                        <input type="text" class="form-control" id="signup-mobilenumber">
                                    </div>
                                    <div class="form-group cust-mt">
                                        <label for="signup-number">Your Email Address</label>
                                        <input type="email" class="form-control" id="signup-number">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="grade">Grade</label>
                                        <select class="form-control" id="grade">
                                            <option value=""></option>
                                            <option value="">8th</option>
                                            <option value="">10th</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="signup-password">Password</label>
                                        <input type="password" class="form-control" id="signup-password">
                                        <small id="password-help" class="form-text text-muted">Passwords should be at least 8 characters long and should contain a mixture of letters, numbers, and other characters.</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="signup-confirmpassword">Confirm Password</label>
                                        <input type="password" class="form-control" id="signup-confirmpassword">
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="col-6 offset-3 btn btn-primary btn-gradient" onclick="gotosignupprocess2();">Sign Up</button>
                        </form>
                    </div>
                    <div class="card-footer text-center">
                        <p>Already have an account? <a href="<?= $baseurl; ?>" class="cust-link">Sign In</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sign-up-process2 d-none">
        <div class="vh-100 d-flex align-items-center">
            <div class="col-4 offset-4 email-verification">
                <div class="card">
                    <div class="card-header text-center">
                        <img src="<?= $baseurl; ?>dest/images/logo.svg" alt="">
                    </div>
                    <div class="card-body text-center">
                        <h4 class="text-center">Create Account</h4>
                        <img class="success-arrow" src="<?= $baseurl; ?>dest/images/icons/successful.svg" alt="">
                        <p class="success-message p-5">A verification link was sent to your email<br> ni******@***il.com<br>Kindly click and verify to proceed with the registration process.</p>
                    </div>
                    <div class="card-footer text-center">
                        <p>Unable to Find Verification Link? <a href="<?= $baseurl; ?>sign-up/" class="cust-link">Resend Email</a></p>
                    </div>
                </div>
            </div>
            <div class="col-4 offset-4 otp-verification">
                <div class="card">
                    <div class="card-header text-center">
                        <img src="<?= $baseurl; ?>dest/images/logo.svg" alt="">
                    </div>
                    <div class="card-body text-center">
                        <h4 class="text-center">Create Account</h4>
                        <p class="success-message pb-5 pt-3">A 6 digit OTP was sent to your mobile no.<br> 98*******73<br>Kindly enter below and verify to proceed with the registration process.</p>
                        <form class="text-left">
                            <div class="form-group">
                                <label for="signup-number">Enter 6 digit OTP</label>
                                <input type="text" class="form-control" id="signup-number">
                            </div>
                            <button type="button" class="btn btn-primary btn-gradient" onclick="gotosignupprocess3();">Sign Up</button>
                        </form>
                    </div>
                    <div class="card-footer text-center">
                        <p>Did not receive OTP? <a href="<?= $baseurl; ?>sign-up/" class="cust-link">Resend OTP</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section class="sign-up-process3">
        <div class="vh-100 d-flex align-items-center">
            <div class="col-8 offset-2">
                <div class="card">
                    <div class="card-header text-center">
                        <img src="<?= $baseurl; ?>images/logo.svg" alt="">
                    </div>
                    <div class="card-body">
                        <div class="steps">
                            <div class="stepLine"></div>
                            <ul class="steps">
                                <li class="completedStep"><p>1</p></li>
                                <li><p>2</p></li>
                                <li><p>3</p></li>
                                <li><p>4</p></li>
                                <li><p>5</p></li>
                            </ul>
                        </div>
                        <h4 class="text-center">Create Account</h4>
                        <form>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="signup-fullname">Full Name</label>
                                    <input type="text" class="form-control" id="signup-fullname">
                                </div>
                                <div class="form-group col-6">
                                    <label for="grade">Grade</label>
                                    <select class="form-control" id="grade">
                                        <option value=""></option>
                                        <option value="">8th</option>
                                        <option value="">10th</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="country">Country</label>
                                    <select class="form-control" id="country">
                                        <option value=""></option>
                                        <option value="">India</option>
                                        <option value="">Country Name</option>
                                    </select>
                                </div>
                                <div class="form-group col-6">
                                    <label for="city">Country</label>
                                    <select class="form-control" id="city">
                                        <option value=""></option>
                                        <option value="">Mumbai</option>
                                        <option value="">Delhi</option>
                                    </select>
                                </div>
                            </div>
                            <button type="button" class="col-6 offset-3 btn btn-primary btn-gradient"onclick="gotosignupprocess4();">Sign Up</button>
                        </form>
                    </div>
                    <div class="card-footer text-center">
                        <p>Already have an account? <a href="<?= $baseurl; ?>" class="cust-link">Sign In</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <section class="sign-up-process3 d-block">
        <div class="vh-100 d-flex align-items-center">
            <div class="col-8 offset-2">
                <div class="card">
                    <div class="card-header card-header-title">
                        <div class="container">
                            <div class="row">
                                <div class="col-8">
                                    <div class="tab-wrapper align-items-center">
                                        <a href="javascript:;" class="btn-prev mr-3"><img src="<?= $baseurl; ?>dest/images/icons/arrow-prev.svg" alt=""></a>
                                        <div class="tab-header tab-header-course " id="tab1">
                                            <span><h5>Select Course</h5></span>
                                        </div>
                                        <div class="tab-header tab-header-course" id="tab2">
                                            <span><h5>Study Schedule</h5></span>
                                        </div>
                                        <div class="tab-header tab-header-course tab-header-checked active" id="tab3">
                                            <span><h5>Payment</h5></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="steps">
                                        <div class="stepLine"></div>
                                        <ul class="steps">
                                            <li class="completedStep"><p>1</p></li>
                                            <li><p>2</p></li>
                                            <li><p>3</p></li>
                                            <li><p>4</p></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-body tab1 active" style="display:none">
                            <div class="card-body-head container">
                                <h5 class=" ">Select the Course you want to study for.</h5>
                                <p>You can select more than one.</p>
                            </div>
                            <form>
                                <div class="container">
                                    <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label class="checkbox-wrap checkbox-container checked">
                                                <div class="row align-items-center">
                                                    <div class="col-7">
                                                        <h5 class="course-title">SAT</h5>
                                                        <input type="checkbox" id="satCourse" name="selectCourse" checked>
                                                        <span class="checkmark"></span>
                                                    </div>
                                                    <div class="col-5 check-rate text-right">
                                                        <h5>&#8377; 5,000</h5>
                                                        <p>till exam dates</p>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="checkbox-wrap checkbox-container checked">
                                                <div class="row align-items-center">
                                                    <div class="col-7">
                                                        <h5 class="course-title">IB Math</h5>
                                                        <input type="checkbox" id="ibMathCourse" name="selectCourse" checked>
                                                        <span class="checkmark"></span>
                                                    </div>
                                                    <div class="col-5 check-rate text-right">
                                                        <h5>&#8377; 5,000</h5>
                                                        <p>till exam dates</p>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="checkbox-wrap checkbox-container">
                                                <div class="row align-items-center">
                                                    <div class="col-7">
                                                        <h5 class="course-title">ACT</h5>
                                                        <input type="checkbox" id="actCourse" name="selectCourse">
                                                        <span class="checkmark"></span>
                                                    </div>
                                                    <div class="col-5 check-rate text-right">
                                                        <h5>&#8377; 2,500</h5>
                                                        <p>(3 months access)</p>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="checkbox-wrap checkbox-container">
                                                <div class="row align-items-center">
                                                    <div class="col-7">
                                                        <h5 class="course-title">IB Physics</h5>
                                                        <input type="checkbox" id="ibPhysicsCourse" name="selectCourse">
                                                        <span class="checkmark"></span>
                                                    </div>
                                                    <div class="col-5 check-rate text-right">
                                                        <h5>&#8377; 2,500</h5>
                                                        <p>(3 months access)</p>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="checkbox-wrap checkbox-container">
                                                <div class="row align-items-center">
                                                    <div class="col-7">
                                                        <h5 class="course-title">Study Abroad Counselling</h5>
                                                        <input type="checkbox" id="studyAbroadCourse" name="selectCourse">
                                                        <span class="checkmark"></span>
                                                    </div>
                                                    <div class="col-5 check-rate text-right">
                                                        <h5>&#8377; 2,500</h5>
                                                        <p>(3 months access)</p>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="checkbox-wrap checkbox-container">
                                                <div class="row align-items-center">
                                                    <div class="col-7">
                                                        <h5 class="course-title">AP Calculus</h5>
                                                        <input type="checkbox" id="studyAbroadCourse" name="selectCourse">
                                                        <span class="checkmark"></span>
                                                    </div>
                                                    <div class="col-5 check-rate text-right">
                                                        <h5>&#8377; 1,800</h5>
                                                        <p>till exam dates</p>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="promocode">Enter PromoCode</label>
                                                <input type="text" class="form-control" id="promocode">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <p class="text-right text-custom-secondary d-flex justify-content-end align-items-center cursor-pointer" onclick="gotostudyschedule()">Next  <a href="javascript:;" class="btn-next d-flex align-items-center ml-2"><img src="<?= $baseurl; ?>dest/images/icons/arrow-next.svg" alt=""> </a></p>
                                        </div>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                        <div class="tab-body tab2" style="display:none">
                            <div class="container">
                                <div class="row">
                                    <div class="col-8">
                                        <div class="tab-wrapper tab-wrapper-study">
                                            <div class="tab-header tab-header-study active" id="tabStudy1">
                                                <span><h5>SAT</h5></span>
                                            </div>
                                            <div class="tab-header tab-header-study" id="tabStudy2">
                                                <span><h5>New</h5></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <p class="text-right text-custom-blue d-flex justify-content-end align-items-center cursor-pointer">Skip to Dashboard  <a href="javascript:;" class="btn-next d-flex align-items-center ml-2"><img src="<?= $baseurl; ?>dest/images/icons/blue-right-arrow.svg" alt=""> </a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-study-body tabStudy1 active">
                                <form>
                                    <div class="container selectcoursedate">
                                        <div class="row">
                                            <div class="col-12">
                                                <img src="<?= $baseurl; ?>dest/images/icons/study-schedule.svg" alt="">
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="exam-date">Enter Your Sat Exam Date (Can be Tentative)</label>
                                                    <input type="text" class="form-control date-input" id="exam-date1">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <p class="text-right text-custom-secondary d-flex justify-content-end align-items-center cursor-pointer" onclick="gotoselectcoursedays()">Next <a href="javascript:;" class="btn-next d-flex align-items-center ml-2"><img src="<?= $baseurl; ?>dest/images/icons/arrow-next.svg" alt=""> </a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container selectcoursedays">
                                        <div class="row">
                                            <div class="col-4">
                                                <p>Practice Test 1</p>
                                                <div class="form-group">
                                                    <label for="exam-date">Select Day</label>
                                                    <input type="text" class="form-control date-input" id="practice-date1">
                                                </div>
                                            </div>
                                            <div class="col-2"></div>
                                            <div class="col-4">
                                                <p>Practice Test 2</p>
                                                <div class="form-group">
                                                    <label for="exam-date">Select Day</label>
                                                    <input type="text" class="form-control date-input" id="practice-date2">
                                                </div>
                                            </div>
                                            <div class="col-2"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 offset-3">
                                                <a class="btn d-flex align-items-center justify-content-center addTestBtn"><img class="mr-2" src="<?= $baseurl; ?>dest/images/icons/add.svg" alt="">Add More Practice Test</a>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-6">
                                                <p class="d-flex"><a href="javascript:;" class="btn-next d-flex align-items-center mr-2"><img src="<?= $baseurl; ?>dest/images/icons/back.svg" alt=""> </a>Edit Tentative Date of SAT Exam</p>
                                            </div>
                                            <div class="col-6">
                                                <p class="text-right text-custom-secondary d-flex justify-content-end align-items-center cursor-pointer" onclick="gotostudydays()">Next  <a href="javascript:;" class="btn-next d-flex align-items-center ml-2"><img src="<?= $baseurl; ?>dest/images/icons/arrow-next.svg" alt=""> </a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container selectstudydays">
                                        <div class="row">
                                            <div class="col-12">
                                                <p class="note mb-3">For the best results our algorithm suggests<br><span>16 hrs/week study time</span> for the <span>remaining 8 weeks</span><br>The recommendations are inserted automatically below.</p>
                                                <label class="schedule-title">Select the days of study</label> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="option-wrapper">
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="monday" value="Part - Time" checked>
                                                        <label class="option-flat-button" for="monday">M</label>
                                                    </label>
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="tuesday" value="Full - Time">
                                                        <label class="option-flat-button" for="tuesday">T</label>
                                                    </label>
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="wednesday" value="Work From Home" checked>
                                                        <label class="option-flat-button" for="wednesday">W</label>
                                                    </label>
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="thursday" value="Work From Home">
                                                        <label class="option-flat-button" for="thursday">T</label>
                                                    </label>
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="friday" value="Work From Home">
                                                        <label class="option-flat-button" for="friday">F</label>
                                                    </label>
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="saturday" value="Work From Home" checked>
                                                        <label class="option-flat-button" for="saturday">S</label>
                                                    </label>
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="sunday" value="Work From Home" checked>
                                                        <label class="option-flat-button" for="sunday">S</label>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="row">
                                                    <div class="form-group col-6 pr-5">
                                                        <label for="study-hours">Study Hours / Day</label>
                                                        <select class="form-control" id="study-hours">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-6 pr-5">
                                                        <label for="city">Notify Me from time</label>
                                                        <select class="form-control" id="city">
                                                            <option value="12:30 PM onwards">12:30 PM onwards</option>
                                                            <option value="2.00 PM onwards">2.00 PM onwards</option>
                                                            <option value="4.00 PM onwards">4.00 PM onwards</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-6">
                                                <p class="d-flex"><a href="javascript:;" class="btn-next d-flex align-items-center mr-2"><img src="<?= $baseurl; ?>dest/images/icons/back.svg" alt=""> </a>Edit Tentative Date of SAT Exam</p>
                                            </div>
                                            <div class="col-6">
                                                <p class="text-right text-custom-secondary d-flex justify-content-end align-items-center cursor-pointer" onclick="gotonextcourse()">Next  <a href="javascript:;" class="btn-next d-flex align-items-center ml-2"><img src="<?= $baseurl; ?>dest/images/icons/arrow-next.svg" alt=""> </a></p>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-study-body tabStudy2">
                                <form>
                                    <div class="container selectcoursedate2">
                                        <div class="row">
                                            <div class="col-12">
                                                <img src="<?= $baseurl; ?>dest/images/icons/study-schedule.svg" alt="">
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="exam-date">Enter Your Sat Exam Date (Can be Tentative)</label>
                                                    <input type="text" class="form-control date-input" id="exam-date2">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <p class="text-right text-custom-secondary d-flex justify-content-end align-items-center cursor-pointer" onclick="gotoselectcoursedays2()">Next <a href="javascript:;" class="btn-next d-flex align-items-center ml-2"><img src="<?= $baseurl; ?>dest/images/icons/arrow-next.svg" alt=""> </a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container selectcoursedays2">
                                        <div class="row">
                                            <div class="col-4">
                                                <p>Practice Test 1</p>
                                                <div class="form-group">
                                                    <label for="exam-date">Select Day</label>
                                                    <input type="text" class="form-control date-input" id="practice-date1">
                                                </div>
                                            </div>
                                            <div class="col-2"></div>
                                            <div class="col-4">
                                                <p>Practice Test 2</p>
                                                <div class="form-group">
                                                    <label for="exam-date">Select Day</label>
                                                    <input type="text" class="form-control date-input" id="practice-date2">
                                                </div>
                                            </div>
                                            <div class="col-2"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 offset-3">
                                                <a class="btn d-flex align-items-center justify-content-center addTestBtn"><img class="mr-2" src="<?= $baseurl; ?>dest/images/icons/add.svg" alt="">Add More Practice Test</a>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-6">
                                                <p class="d-flex"><a href="javascript:;" class="btn-next d-flex align-items-center mr-2"><img src="<?= $baseurl; ?>dest/images/icons/back.svg" alt=""> </a>Edit Tentative Date of SAT Exam</p>
                                            </div>
                                            <div class="col-6">
                                                <p class="text-right text-custom-secondary d-flex justify-content-end align-items-center cursor-pointer" onclick="gotostudydays2()">Next  <a href="javascript:;" class="btn-next d-flex align-items-center ml-2"><img src="<?= $baseurl; ?>dest/images/icons/arrow-next.svg" alt=""> </a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container selectstudydays2">
                                        <div class="row">
                                            <div class="col-12">
                                                <p class="note mb-3">For the best results our algorithm suggests<br><span>16 hrs/week study time</span> for the <span>remaining 8 weeks</span><br>The recommendations are inserted automatically below.</p>
                                                <label class="schedule-title">Select the days of study</label> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="option-wrapper">
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="monday" value="Part - Time" checked>
                                                        <label class="option-flat-button" for="monday">M</label>
                                                    </label>
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="tuesday" value="Full - Time">
                                                        <label class="option-flat-button" for="tuesday">T</label>
                                                    </label>
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="wednesday" value="Work From Home" checked>
                                                        <label class="option-flat-button" for="wednesday">W</label>
                                                    </label>
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="thursday" value="Work From Home">
                                                        <label class="option-flat-button" for="thursday">T</label>
                                                    </label>
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="friday" value="Work From Home">
                                                        <label class="option-flat-button" for="friday">F</label>
                                                    </label>
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="saturday" value="Work From Home" checked>
                                                        <label class="option-flat-button" for="saturday">S</label>
                                                    </label>
                                                    <label class="option-inline">
                                                        <input type="checkbox" class="option-button" name="select-days" id="sunday" value="Work From Home" checked>
                                                        <label class="option-flat-button" for="sunday">S</label>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="row">
                                                    <div class="form-group col-6 pr-5">
                                                        <label for="study-hours">Study Hours / Day</label>
                                                        <select class="form-control" id="study-hours">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-6 pr-5">
                                                        <label for="city">Notify Me from time</label>
                                                        <select class="form-control" id="city">
                                                            <option value="12:30 PM onwards">12:30 PM onwards</option>
                                                            <option value="2.00 PM onwards">2.00 PM onwards</option>
                                                            <option value="4.00 PM onwards">4.00 PM onwards</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-6">
                                                <p class="text-custom-secondary d-flex"><a href="javascript:;" class="btn-next d-flex align-items-center mr-2"><img src="<?= $baseurl; ?>dest/images/icons/arrow-prev.svg" alt=""> </a>Edit Tentative Date of SAT Exam</p>
                                            </div>
                                            <div class="col-6">
                                                <a href="<?= $baseurl; ?>dashboard/"><p class="text-right text-custom-secondary d-flex justify-content-end align-items-center cursor-pointer">Start Course Now<a href="javascript:;" class="btn-next d-flex align-items-center ml-2"><img src="<?= $baseurl; ?>dest/images/icons/arrow-next.svg" alt=""> </a></p></a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-body tab3">
                            <div class="card-body-head container">
                                <h5 class=" ">Selected Courses</h5>
                            </div>
                            <form>
                                <div class="container">
                                    <div class="row ">
                                        <div class="form-group col-md-6">
                                            <label class="checkbox-wrap checkbox-container checked">
                                                <div class="row align-items-center">
                                                    <div class="col-7">
                                                        <h5 class="course-title">SAT</h5>
                                                        <input type="checkbox" id="paymentsatCourse" name="paymnetSelectCourse" checked>
                                                        <span class="checkmark"></span>
                                                    </div>
                                                    <div class="col-5 check-rate text-right">
                                                        <h5>&#8377; 5,000</h5>
                                                        <p>till exam dates</p>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="checkbox-wrap checkbox-container checked">
                                                <div class="row align-items-center">
                                                    <div class="col-7">
                                                        <h5 class="course-title">IB Math</h5>
                                                        <input type="checkbox" id="paymentibMathCourse" name="paymnetSelectCourse" checked>
                                                        <span class="checkmark"></span>
                                                    </div>
                                                    <div class="col-5 check-rate text-right">
                                                        <h5>&#8377; 5,000</h5>
                                                        <p>till exam dates</p>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 text-center paymnet-details">
                                            <h6>Total Amount Payable</h6>
                                            <h2>&#8377; 10,000</h2>
                                            <p class="col-4  offset-4 text-center">This will be auto debited from your payment option.</p>
                                        </div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="promocode">Enter PromoCode</label>
                                                <input type="text" class="form-control" id="promocode">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <p class="text-right text-custom-secondary d-flex justify-content-end align-items-center cursor-pointer" onclick="gotostudyschedule()">Next  <a href="javascript:;" class="btn-next d-flex align-items-center ml-2"><img src="<?= $baseurl; ?>dest/images/icons/arrow-next.svg" alt=""> </a></p>
                                        </div>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    include 'footer.php';
    ?>
    <script src="<?= $baseurl; ?>dest/js/datepicker.js" defer></script>
    <script src="<?= $baseurl; ?>dest/js/intlTelInput.js" defer></script>
    <script>
        $(function() {
            $('#exam-date1,#exam-date2,#practice-date1,#practice-date2').datepicker();
            $("#signup-mobilenumber").intlTelInput({
                preferredCountries: ["in" ],
                separateDialCode: true
            });
        });
    </script>
</body>

</html>