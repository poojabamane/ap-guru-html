<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include 'head.php';
        ?>
        <title>Journey | AP Guru</title>
        <meta name="description" content="">
    </head>
    <body class="active-page" id="journey-page">
        <?php
            include 'header.php';
            include 'sidebar.php';
        ?>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-8">
                    <div class="row">
                        <div class="col-4">
                            <div class="d-flex align-items-center">
                                <a href="#" class="add-back question-control mr-4">
                                    <img src="/ap-guru-html/dest/images/icons/prev-icon.svg" alt="">
                                </a>
                                <select class="form-control text-uppercase question-filter" name="" id="">
                                    <option value="unanswered">unanswered</option>
                                </select>
                                <a href="#" class="add-next question-control ml-4">
                                    <img src="/ap-guru-html/dest/images/icons/next-icon.svg" alt="">
                                </a>
                                
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="d-flex flex-wrap test-topics">
                                <p>Section: <span class="NunitoSans-Bold">Maths</span></p>
                                <p>Difficulty : <span class="NunitoSans-Bold">1</span></p>
                                <p>Topic : <span class="NunitoSans-Bold">Number Properties</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="d-flex">
                        <a href="#" class="btn btn-outline-primary btn-outline-blue mr-2">Back To Report</a>
                        <a href="#" class="btn btn-primary btn-gradient-blue ml-2">Back To Section</a>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-7">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-question">
                                <p class="NunitoSans-Bold">1. Marcus’s favorite casserole recipe requires 3 eggs and makes 6 servings. Marcus will modify the recipe by using 5 eggs and increasing all other ingredients in the recipe proportionally. What is the total number of servings the modified recipe will make ?</p>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="card card-question">
                                <p class="NunitoSans-Bold mb-3">Video Explanation</p>
                                <div class="w-100">
                                    <div class="video-box">
                                        <img src="<?= $baseurl; ?>dest/images/resourses/video.jpg" alt="" class="img-fluid">
                                        <img src="<?= $baseurl; ?>dest/images/icons/video-play.svg" alt="" class="video-play">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="card card-question">
                                <p class="NunitoSans-Bold mb-3">Read Explanation</p>
                                <p>Answer C</p>
                                <div class="explanation-box">
                                    <img src="<?= $baseurl; ?>dest/images/resourses/explation.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-5">
                    <div class="question-choice">
                        <div class="row">
                            <div class="form-group col-12">
                                <label class="checkbox-wrap checkbox-container checked-incorrect checked">
                                    <div class="row align-items-center">
                                        <div class="col-6">
                                            <h6 class="check-title">6</h6>
                                            <input type="checkbox" id="actCourse" name="questionOption" checked>
                                            <span class="radiomark"></span>
                                        </div>
                                        <div class="col-6 check-rate text-right">
                                            <p>Your Answer</p>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="form-group col-12">
                                <label class="checkbox-wrap checkbox-container">
                                    <div class="row align-items-center">
                                        <div class="col-6">
                                            <h6 class="check-title">8</h6>
                                            <input type="checkbox" id="actCourse" name="questionOption">
                                            <span class="radiomark"></span>
                                        </div>
                                        <div class="col-6 check-rate text-right">
                                            <p></p>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="form-group col-12">
                                <label class="checkbox-wrap checkbox-container checked-correct checked">
                                    <div class="row align-items-center">
                                        <div class="col-6">
                                            <h6 class="check-title">10</h6>
                                            <input type="checkbox" id="actCourse" name="questionOption" checked>
                                            <span class="radiomark"></span>
                                        </div>
                                        <div class="col-6 check-rate text-right">
                                            <p>Correct Answer</p>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="form-group col-12">
                                <label class="checkbox-wrap checkbox-container">
                                    <div class="row align-items-center">
                                        <div class="col-6">
                                            <h6 class="check-title">12</h6>
                                            <input type="checkbox" id="actCourse" name="questionOption" checked>
                                            <span class="radiomark"></span>
                                        </div>
                                        <div class="col-6 check-rate text-right">
                                            <p></p>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="form-group col-12">
                                <label class="checkbox-wrap checkbox-container">
                                    <div class="row align-items-center">
                                        <div class="col-6">
                                            <h6 class="check-title">15</h6>
                                            <input type="checkbox" id="actCourse" name="questionOption">
                                            <span class="radiomark"></span>
                                        </div>
                                        <div class="col-6 check-rate text-right">
                                            <p></p>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            include 'footer.php';
        ?>
    </body>
</html>