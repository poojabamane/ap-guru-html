<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'head.php';
    ?>
    <title>Journey | AP Guru</title>
    <meta name="description" content="">
</head>

<body class="active-page" id="journey-page">
    <?php
    include 'header.php';
    include 'sidebar.php';
    ?>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 d-flex">
                <div class="test-tab w-100">
                    <div class="tab-pane d-flex flex-wrap">
                        <div class="tab-title active active-bg-blue">
                            <div class="tab-title-main">
                                <h5>Overall</h5>
                            </div>
                        </div>
                        <div class="tab-title active-bg-blue">
                           <div class="d-flex justify-content-between align-items-end">
                                <h5>English</h5>
                                <h6 class="test-progress text-slow">77%</h6>
                           </div>
                           <div class="progress-control">
                                <div class="progress progress-linear">
                                    <div class="progress-linear-bar" role="progressbar" style="width: 77%" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                           </div>
                        </div>
                        <div class="tab-title active-bg-red">
                           <div class="d-flex justify-content-between align-items-end">
                                <h5>Maths</h5>
                                <h6 class="test-progress text-slow">66%</h6>
                           </div>
                           <div class="progress-control">
                                <div class="progress progress-linear">
                                    <div class="progress-linear-bar" role="progressbar" style="width: 66%" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                           </div>
                        </div>
                        <div class="tab-title active-bg-green">
                           <div class="d-flex justify-content-between align-items-end">
                                <h5>Reading</h5>
                                <h6 class="test-progress text-slow">50%</h6>
                           </div>
                           <div class="progress-control">
                                <div class="progress progress-linear">
                                    <div class="progress-linear-bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                           </div>
                        </div>
                        <div class="tab-title active-bg-orange">
                           <div class="d-flex justify-content-between align-items-end">
                                <h5>Science</h5>
                                <h6 class="test-progress text-slow">90%</h6>
                           </div>
                           <div class="progress-control">
                                <div class="progress progress-linear">
                                    <div class="progress-linear-bar" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                           </div>
                        </div>
                    </div>
                    <div class="tab-details mt-3">
                        <div class="row">
                            <div class="col-2">
                                <div class="card icon-card text-center">
                                    <img src="<?= $baseurl; ?>dest/images/icons/journey/target.svg" alt="" class="mb-2">
                                    <h3 class="card-score">66.6%</h3>
                                    <p class="card-text">Average Accuracy</p>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="card icon-card text-center">
                                    <img src="<?= $baseurl; ?>dest/images/icons/journey/quiz.svg" alt="" class="mb-2">
                                    <h3 class="card-score">32<span class="card-sub-score">/64</span></h3>
                                    <p class="card-text">Quiz Completed</p>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="card icon-card text-center">
                                    <img src="<?= $baseurl; ?>dest/images/icons/journey/test.svg" alt="" class="mb-2">
                                    <h3 class="card-score">07<span class="card-sub-score">/15</span></h3>
                                    <p class="card-text">Test Completed</p>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="card icon-card text-center">
                                    <img src="<?= $baseurl; ?>dest/images/icons/journey/digital-books.svg" alt="" class="mb-2">
                                    <h3 class="card-score">20<span class="card-sub-score">/64</span></h3>
                                    <p class="card-text">Digital Books Completed</p>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="card icon-card text-center">
                                    <img src="<?= $baseurl; ?>dest/images/icons/journey/avg-test-score.svg" alt="" class="mb-2">
                                    <h3 class="card-score">72<span class="card-sub-score">/100</span></h3>
                                    <p class="card-text">Avg. Test Score</p>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="card icon-card text-center">
                                    <img src="<?= $baseurl; ?>dest/images/icons/journey/avg-last-test.svg" alt="" class="mb-2">
                                    <h3 class="card-score">05<span class="card-sub-score">/05</span></h3>
                                    <p class="card-text">Avg. Test Score (Last 5)</p>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="card icon-card text-center">
                                    <img src="<?= $baseurl; ?>dest/images/icons/journey/active-hrs.svg" alt="" class="mb-2">
                                    <h3 class="card-score">22:30</span></h3>
                                    <p class="card-text">Active Study Hours</p>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="card icon-card text-center">
                                    <img src="<?= $baseurl; ?>dest/images/icons/journey/active-time.svg" alt="" class="mb-2">
                                    <h3 class="card-score">15s</h3>
                                    <p class="card-text">Avg. Time Per Question</p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <?php
    include 'footer.php';
    ?>
</body>

</html>