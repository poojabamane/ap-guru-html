<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include 'head.php';
        ?>
        <title>Journey | AP Guru</title>
        <meta name="description" content="">
    </head>
    <body class="active-page" id="journey-page">
        <?php
            include 'header.php';
            include 'sidebar.php';
        ?>
            <div class="content-wrapper">
                <div class="row ">
                    <div class="col-12 d-flex align-items-center">
                        <a href="#" class="add-back mr-2">
                            <img src="<?= $baseurl; ?>dest/images/icons/back-button.svg" alt="">
                        </a>    
                        <h3>Test 16 - Detailed Report</h3>
                    </div>
                </div>  
                <div class="row mt-4">
                    <div class="col-12 d-flex">
                        <div class="test-tab test-details-tab w-100">
                            <div class="tab-pane d-flex flex-wrap">
                                <div class="container-fluid no-padding">
                                    <div class="row d-flex align-items-center">
                                        <div class="col-10 d-flex">
                                            <div class="tab-title active-bg-blue">
                                                <h5 class="text-blue">English</h5>
                                            </div>
                                            <div class="tab-title active active-bg-orange">
                                                <h5 class="text-orange">Maths</h5>
                                            </div>
                                            <div class="tab-title active-bg-green">
                                                <h5 class="text-green">Reading</h5>
                                            </div>
                                            <div class="tab-title active-bg-red">
                                                <h5 class="text-red">Science</h5>
                                            </div>
                                        </div>
                                        <div class="col-2 text-center">
                                            <select name="question-filter" id="question-filter" class="form-control text-uppercase question-filter">
                                                <option value="1">All questions</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-details mt-4">
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <div class="card card-box tests">
                                            <div class="card-header">
                                                <div class="d-flex">
                                                    <h5>Time Per Question</h5>
                                                </div>
                                            </div>
                                            <div class="card-body"></div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="card card-box tests">
                                            <div class="card-header">
                                                <div class="d-flex">
                                                    <h5>Weakest Topics</h5>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                
                                                    <div class="course-wrap w-100 scrollbar">
                                                        <div class="topic-details-list d-flex align-items-center justify-content-between">
                                                            <h5>Sentence Structures</h5>
                                                            <a href="#">
                                                                <img src="<?= $baseurl; ?>dest/images/icons/next-green.svg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="topic-details-list d-flex align-items-center justify-content-between">
                                                            <h5>Punctuations</h5>
                                                            <a href="#">
                                                                <img src="<?= $baseurl; ?>dest/images/icons/next-green.svg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="topic-details-list d-flex align-items-center justify-content-between">
                                                            <h5>Pronouns</h5>
                                                            <a href="#">
                                                                <img src="<?= $baseurl; ?>dest/images/icons/next-green.svg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="topic-details-list d-flex align-items-center justify-content-between">
                                                            <h5>Verbs</h5>
                                                            <a href="#">
                                                                <img src="<?= $baseurl; ?>dest/images/icons/next-green.svg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="topic-details-list d-flex align-items-center justify-content-between">
                                                            <h5>Parallelism</h5>
                                                            <a href="#">
                                                                <img src="<?= $baseurl; ?>dest/images/icons/next-green.svg" alt="">
                                                            </a>
                                                        </div>
                                                    </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-12">
                                        
                                            <table class="table table-striped table-bordered table-jouney-details text-uppercase text-center">
                                                <thead>
                                                    <tr>
                                                        <th><p>Question No.</p></th>
                                                        <th><p>Correct Answer</p></th>
                                                        <th><p>Your Answer</p></th>
                                                        <th colspan="2"><p>Explanation</p></th>
                                                        <th><p>Difficulty</p></th>
                                                        <th><p>Topics</p></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><a href="#"><p>1</p></a></td>
                                                        <td><p>C </p></td>
                                                        <td><p>C <span class="ml-1 text-green">&#10003;</span></p></td>
                                                        <td><a href="#"><img src="<?= $baseurl; ?>dest/images/icons/info.svg" alt=""></a></td>
                                                        <td><a href="javascript:;" class="checkVideo"><img src="<?= $baseurl; ?>dest/images/icons/video.svg" alt=""></a></td>
                                                        <td><p>1</p></td>
                                                        <td><p>Number Properties</p></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="#"><p>2</p></a></td>
                                                        <td><p>E</p></td>
                                                        <td><p>A <span class="ml-1 text-red">&#10005;</span></td>
                                                        <td><a href="#"><img src="<?= $baseurl; ?>dest/images/icons/info.svg" alt=""></a></td>
                                                        <td><a href="#"><img src="<?= $baseurl; ?>dest/images/icons/video.svg" alt=""></a></td>
                                                        <td><p>2</p></td>
                                                        <td><p>Probability</p></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="#"><p>3</p></a></td>
                                                        <td><p>E</p></td>
                                                        <td><p><span class="text-blue">&mdash;</span></p></td>
                                                        <td><a href="#"><img src="<?= $baseurl; ?>dest/images/icons/info.svg" alt=""></a></td>
                                                        <td><a href="#"><img src="<?= $baseurl; ?>dest/images/icons/video.svg" alt=""></a></td>
                                                        <td><p>3</p></td>
                                                        <td><p>Functions</p></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="#"><p>4</p></a></td>
                                                        <td><p>C </p></td>
                                                        <td><p>C <span class="ml-1 text-green">&#10003;</span></p></td>
                                                        <td><a href="#"><img src="<?= $baseurl; ?>dest/images/icons/info.svg" alt=""></a></td>
                                                        <td><a href="#"><img src="<?= $baseurl; ?>dest/images/icons/video.svg" alt=""></a></td>
                                                        <td><p>1</p></td>
                                                        <td><p>Number Properties</p></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="#"><p>5</p></a></td>
                                                        <td><p>E</p></td>
                                                        <td><p>A <span class="ml-1 text-red">&#10005;</span></td>
                                                        <td><a href="#"><img src="<?= $baseurl; ?>dest/images/icons/info.svg" alt=""></a></td>
                                                        <td><a href="#"><img src="<?= $baseurl; ?>dest/images/icons/video.svg" alt=""></a></td>
                                                        <td><p>2</p></td>
                                                        <td><p>Probability</p></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="#"><p>6</p></a></td>
                                                        <td><p>E</p></td>
                                                        <td><p><span class="text-blue">&mdash;</span></p></td>
                                                        <td><a href="#"><img src="<?= $baseurl; ?>dest/images/icons/info.svg" alt=""></a></td>
                                                        <td><a href="#"><img src="<?= $baseurl; ?>dest/images/icons/video.svg" alt=""></a></td>
                                                        <td><p>3</p></td>
                                                        <td><p>Functions</p></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        <?php
            include 'popup.php';
            include 'footer.php';
        ?>
    </body>
</html>