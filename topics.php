<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'head.php';
    ?>
    <title>Topics | AP Guru</title>
    <meta name="description" content="">
</head>

<body class="active-page" id="topics-page">
    <?php
    include 'header.php';
    include 'sidebar.php';
    ?>
    <div class="content-wrapper">
        <div class="row">
            <div class="topic-details d-flex w-100">
                <div class="col-2">
                    <div class="topic-item">
                        <h4>English <span>24/30</span></h4>
                    </div>
                </div>
                <div class="col-2">
                    <div class="topic-item">
                        <h4>English <span>24/30</span></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    include 'footer.php';
    ?>
</body>

</html>