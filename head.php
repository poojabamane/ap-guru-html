<?php
$baseurl = '/ap-guru-html/';
?>
<noscript>
    <meta http-equiv="refresh" content="">
</noscript>
<meta charset="utf-8" />
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<?= $baseurl; ?>dest/images/favicon.png" rel="icon">
<link rel="stylesheet" type="text/css" href="<?= $baseurl; ?>dest/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?= $baseurl; ?>dest/css/style.css">
<!-- Google Analytics -->