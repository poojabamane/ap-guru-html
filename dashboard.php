<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include 'head.php';
        ?>
        <title>Dashboard | AP Guru</title>
        <meta name="description" content="">
    </head>
    <body class="active-page" id="dashboard-page">
        <?php
        include 'header.php';
        include 'sidebar.php';
        ?>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-8">
                    <div class="row">
                        <div class="col-4">
                            <div class="card topics">
                                <div class="card-header">
                                    <h5>Topics</h5>
                                </div>
                                <div class="card-body">
                                    <div class="progress mx-auto" data-value='77'>
                                        <span class="progress-left">
                                            <span class="progress-bar"></span>
                                        </span>
                                        <span class="progress-right">
                                            <span class="progress-bar"></span>
                                        </span>
                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                            <div class="h3">77<sub class="small">%</sub></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-center">
                                    <p>102/148 completed</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="card practice">
                                <div class="card-header">
                                    <h5>Practice</h5>
                                </div>
                                <div class="card-body">
                                    <div class="progress mx-auto" data-value='67'>
                                        <span class="progress-left">
                                            <span class="progress-bar"></span>
                                        </span>
                                        <span class="progress-right">
                                            <span class="progress-bar"></span>
                                        </span>
                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                            <div class="h3">67<sub class="small">%</sub></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-center">
                                    <p>14/28 completed</p>
                                </div>
                            </div>
                        </div><div class="col-4">
                            <div class="card tests">
                                <div class="card-header">
                                    <h5>Tests</h5>
                                </div>
                                <div class="card-body">
                                    <div class="progress mx-auto" data-value='50'>
                                        <span class="progress-left">
                                            <span class="progress-bar"></span>
                                        </span>
                                        <span class="progress-right">
                                            <span class="progress-bar"></span>
                                        </span>
                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                            <div class="h3">50<sub class="small">%</sub></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-center">
                                    <p>14/28 completed</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="card journey">
                                <div class="card-header">
                                    <h5>Resume your Journey</h5>
                                </div>
                                <div class="card-body">
                                    <div class="row d-flex align-items-center">
                                        <div class="col-9">
                                            <!-- <p>Resume your Journey</p> -->
                                            <p class="NunitoSans-Bold mt-2 mb-2">Solving linear equations and linear inequalities</p>
                                            <p>Maths | 30 mins</p>
                                        </div>
                                        <div class="col-3">
                                            <button type="button" class="btn watch-btn">
                                                <img src="<?= $baseurl; ?>dest/images/icons/play.svg" alt="" class="mr-2"> Watch
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="d-flex tabs">
                                        <h5 class="active-tab">English</h5>
                                        <h5>Maths</h5>
                                        <h5>Reading</h5>
                                        <h5>Science</h5>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row topic course-wrap scrollbar">
                                        <div class="col-12 position-relative">
                                            <div class="line"></div>
                                            <div class="row align-items-center pl-5 position-relative topic-block completed-state">
                                                <div class="col-8">
                                                    <div class="d-flex align-items-center">
                                                        <div class="mr-5">
                                                            <h5>Sentence Structures</h5>
                                                            <p class="topic-status">Test Pending</p>
                                                        </div>
                                                        <span class="badge badge-weak">
                                                            <span class="badge-main">Weak</span>
                                                            <span class="badge-sub">12% Accuracy</span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-4 d-flex icon-group">
                                                    <div class="progress-list video  mx-auto" >
                                                        <div class="progress video" data-value='77'>
                                                            <span class="progress-left">
                                                                <span class="progress-bar"></span>
                                                            </span>
                                                            <span class="progress-right">
                                                                <span class="progress-bar"></span>
                                                            </span>
                                                            <div class="progress-value progress-control w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                                <img src="<?= $baseurl; ?>dest/images/icons/video.svg" alt="" class="progress-control-img">
                                                                <span class="progress-control-text">69%</span>
                                                            </div>
                                                        </div>
                                                        <p class="progress-details">Watch</p>
                                                    </div>
                                                    <div class="progress-list test mx-auto" >
                                                        <div class="progress test" data-value='45'>
                                                            <span class="progress-left">
                                                                <span class="progress-bar"></span>
                                                            </span>
                                                            <span class="progress-right">
                                                                <span class="progress-bar"></span>
                                                            </span>
                                                            <div class="progress-value progress-control w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                                <img src="<?= $baseurl; ?>dest/images/icons/pen.svg" alt="" class="progress-control-img">
                                                                <span class="progress-control-text">45%</span>
                                                            </div>
                                                        </div>
                                                        <p class="progress-details">Pratical</p>
                                                    </div>
                                                    <div class="progress-list class mx-auto">
                                                        <div class="progress class" data-value='25'>
                                                            <span class="progress-left">
                                                                <span class="progress-bar"></span>
                                                            </span>
                                                            <span class="progress-right">
                                                                <span class="progress-bar"></span>
                                                            </span>
                                                            <div class="progress-value progress-control w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                                <img src="<?= $baseurl; ?>dest/images/icons/book.svg" alt="" class="progress-control-img">
                                                                <span class="progress-control-text">25%</span>
                                                            </div>
                                                        </div>
                                                        <p class="progress-details">Reading Material</p>
                                                    </div>

                                                    <!-- <div class="progress class mx-auto" data-value='25'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/book.svg" alt="">
                                                        </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row align-items-center pl-5 position-relative topic-block completed-state">
                                                <div class="col-8">
                                                    <div class="d-flex align-items-center">
                                                        <div class="mr-5">
                                                            <h5>Punctuations</h5>
                                                        </div>
                                                        <span class="badge badge-strong">
                                                            <span class="badge-main">Strong</span>
                                                            <span class="badge-sub">80% Accuracy</span>    
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-4 d-flex icon-group">
                                                    <div class="progress video mx-auto" data-value='77'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/video.svg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="progress test mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-pen.svg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="progress class mx-auto" data-value='25'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/book.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row align-items-center pl-5 position-relative topic-block ongoing-state">
                                                <div class="col-8">
                                                    <div class="d-flex align-items-center">
                                                        <div class="mr-5">
                                                            <h5>Pronouns</h5>
                                                        </div>
                                                        <span class="badge badge-avarage">
                                                            <span class="badge-main">Average</span>
                                                            <span class="badge-sub">56% Accuracy</span>        
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-4 d-flex icon-group">
                                                    <div class="progress video mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-video.svg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="progress test mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-pen.svg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="progress class mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-book.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row align-items-center pl-5 position-relative topic-block ">
                                                <div class="col-8">
                                                    <div class="d-flex align-items-center">
                                                        <div class="mr-5">
                                                            <h5>Verbs</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4 d-flex icon-group inactive">
                                                    <div class="progress video mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-video.svg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="progress test mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-pen.svg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="progress class mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-book.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row align-items-center pl-5 position-relative topic-block">
                                                <div class="col-8">
                                                    <div class="d-flex align-items-center">
                                                        <div class="mr-5">
                                                            <h5>Parallelism</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4 d-flex icon-group inactive">
                                                    <div class="progress video mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-video.svg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="progress test mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-pen.svg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="progress class mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-book.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row align-items-center pl-5 position-relative topic-block ">
                                                <div class="col-8">
                                                    <div class="d-flex align-items-center">
                                                        <div class="mr-5">
                                                            <h5>Verbs</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4 d-flex icon-group inactive">
                                                    <div class="progress video mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-video.svg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="progress test mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-pen.svg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="progress class mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-book.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row align-items-center pl-5 position-relative topic-block">
                                                <div class="col-8">
                                                    <div class="d-flex align-items-center">
                                                        <div class="mr-5">
                                                            <h5>Parallelism</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4 d-flex icon-group inactive">
                                                    <div class="progress video mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-video.svg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="progress test mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-pen.svg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="progress class mx-auto" data-value='100'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                            <img src="<?= $baseurl; ?>dest/images/icons/complete-book.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="d-flex tabs">
                                        <h5 class="active-tab">English <span>41%</span></h5>
                                        <h5>Maths<span>74%</span></h5>
                                        <h5>Reading<span>56%</span></h5>
                                        <h5>Science<span>89%</span></h5>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div id="linechart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4"> 
                    <div class="row">
                        <div class="col-12">
                            <div class="card tests">
                                <div class="card-header">
                                    <h5>Schedule</h5>
                                </div>
                                <div class="card-body">
                                    
                                </div>
                                <div class="card-footer text-right">
                                    <p>Edit Schedule ></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="card tests">
                                <div class="card-header">
                                    <h5>Classes</h5>
                                </div>
                                <div class="card-body">
                                    <div class="class-block">
                                        <h5 class="mb-2">Essay Writing : Essay Topic 1</h5>
                                        <p>English</p>
                                        <div class="d-flex justify-content-between">
                                            <p>Chirag Arya</p>
                                            <p>20 mins</p>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2 class-schedule font-weight-bold">
                                            <p>02/12/19  6:00 pm</p>
                                            <p>Schedule ></p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="class-block">
                                        <h5 class="mb-2">Essay Writing : Essay Topic 1</h5>
                                        <p>English</p>
                                        <div class="d-flex justify-content-between">
                                            <p>Chirag Arya</p>
                                            <p>20 mins</p>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2 class-schedule font-weight-bold">
                                            <p>02/12/19  6:00 pm</p>
                                            <p>Schedule ></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-right">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="card tests">
                                <div class="card-header">
                                    <h5>Tests</h5>
                                </div>
                                <div class="card-body">
                                    
                                </div>
                                <div class="card-footer text-right">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="card target p-3">
                                <div class="row justify-content-center align-items-center">
                                    <div class="col-3 text-center">
                                        <img src="<?= $baseurl; ?>dest/images/icons/target.svg" alt="">
                                    </div>
                                    <div class="col-9">
                                        <h5>Create your own quiz</h5>
                                        <p>Start Practicing <img src="<?= $baseurl; ?>dest/images/icons/right-arrow.svg" alt=""></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header d-flex align-items-center justify-content-between">
                                    <h5>Digital Books</h5>
                                    <p>View All</p>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="d-flex resourse-block align-items-center mt-4">
                                        <img src="<?= $baseurl; ?>dest/images/resourses/1.svg" alt="">
                                        <div>
                                            <h5>Formula Sheet</h5>
                                            <p>Maths</p>
                                        </div>
                                    </div>
                                    <div class="d-flex resourse-block align-items-center mt-4">
                                        <img src="<?= $baseurl; ?>dest/images/resourses/2.svg" alt="">
                                        <div>
                                            <h5>List of Authors</h5>
                                            <p>Reading</p>
                                        </div>
                                    </div>
                                    <div class="d-flex resourse-block align-items-center mt-4">
                                        <img src="<?= $baseurl; ?>dest/images/resourses/3.svg" alt="">
                                        <div>
                                            <h5>Basic Tips and Tricks</h5>
                                            <p>Writing</p>
                                        </div>
                                    </div>
                                    <div class="d-flex resourse-block align-items-center mt-4">
                                        <img src="<?= $baseurl; ?>dest/images/resourses/3.svg" alt="">
                                        <div>
                                            <h5>Essay Writing 101</h5>
                                            <p>Writing</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        include 'footer.php';
        ?>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
        <script>
            $(function() {
                $(".progress").each(function() {
                    var value = $(this).attr('data-value');
                    var left = $(this).find('.progress-left .progress-bar');
                    var right = $(this).find('.progress-right .progress-bar');
                    if (value > 0) {
                        if (value <= 50) {
                        right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
                        } else {
                        right.css('transform', 'rotate(180deg)')
                        left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
                        }
                    }
                })
                function percentageToDegrees(percentage) {
                    return percentage / 100 * 360
                }
            });

            // google.charts.load('current', {'packages':['line']});
            // google.charts.setOnLoadCallback(drawChart);

            // function drawChart() {

            //     var data = new google.visualization.DataTable();
            //     data.addColumn('string', '');
            //     data.addColumn('number', '');

            //     data.addRows([
            //         ['Test 1',  25],
            //         ['Test 2',  9],
            //         ['Test 3',  15],
            //         ['Test 4',  35],
            //         ['Test 5',  22],
            //         ['Test 6',  24],
            //     ]);

            //     var options = {
            //         legend: 'none'
            //     }

            //     var chart = new google.charts.Line(document.getElementById('linechart'));

            //     chart.draw(data, google.charts.Line.convertOptions());
            // }

            google.load('visualization', '1.1', {packages: ['line']});
            google.setOnLoadCallback(drawChart);

            function drawChart() {

                var data = new google.visualization.DataTable();
                data.addColumn('string', '');
                data.addColumn('number', '');

                data.addRows([
                    ['Test 1',  25],
                    ['Test 2',  5],
                    ['Test 3',  15],
                    ['Test 4',  35],
                    ['Test 5',  22],
                    ['Test 6',  24],
                ]);

                var options = {
                    legend: {position: 'none'},
                    pointSize: 30,
                    pointShape: 'circle',
                    colors:['#EF9857']
                }

                var chart = new google.charts.Line(document.getElementById('linechart'));

                chart.draw(data, options);
            }
        </script>
    </body>

</html>