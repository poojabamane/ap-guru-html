<div class="preloader"></div>
<header class="position-fixed w-100 z-9">
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand col-2" href="<?= $baseurl; ?>"><img src="<?= $baseurl; ?>dest/images/logo.svg" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="col-10 collapse navbar-collapse" id="menu">
            <form class="col-10 form-inline">
                <input class="form-control col-6 searchbox" type="search" placeholder="Search" aria-label="Search">
            </form>
            <ul class="col-2 justify-content-around align-items-center navbar-nav">
                <li class="nav-item position-relative">
                    <a class="nav-link"><img src="<?= $baseurl; ?>dest/images/icons/notification.svg" alt=""></a>
                    <span class="d-block position-absolute rounded-circle text-center notification-count">5</span>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><img class="rounded-circle" src="<?= $baseurl; ?>dest/images/user.svg" alt=""></a>
                </li>
            </ul>
        </div>
    </nav>
</header>