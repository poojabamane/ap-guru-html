$(document).ready(function() {
  $(".preloader").fadeOut();

  var pageId = $(".active-page").attr("id");
  $("." + pageId).addClass("active-link");

  $('input[name="selectCourse"], input[name="questionOption"]').click(function() {
    if ($(this).is(":checked")) {
      $(this).parent().parent().parent().addClass("checked");
    } 
    else if ($(this).is(":not(:checked)")) {
      $(this).parent().parent().parent().removeClass("checked");
    }
  });

  $('.tab-header-course').click(function(){
    var tab_id=this.id;
    if($(this).hasClass('active')){

    }
    else
    {
        $('.active').parent().find('.tab-body').fadeOut();
        $('.tab-header-course').removeClass('active');
    }
    $('.' + tab_id).delay(400).fadeToggle("slow");
    $(this).toggleClass('active');
  });
  $('.tab-header-study').click(function(){
      var tab_id=this.id;
      if($(this).hasClass('active')){

      }
      else
      {
          $('.active').parent().find('.tab-study-body').fadeOut();
          $('.tab-header-study').removeClass('active');   
      }
      $('.' + tab_id).delay(400).fadeToggle("slow");
      $(this).toggleClass('active');
  });

  $('.checkVideo').click(function(){
    $('.card-popup-video, .page-overlay').fadeIn();
  });
  $('.page-overlay').click(function(){
    $('.card-popup, .page-overlay').fadeOut();
  });
  $('.popup-close').click(function(){
    $('.card-popup, .page-overlay').fadeOut();
  });

  
});

function gotosignupprocess2() {
  $(".sign-up-process1").fadeOut();
  $(".sign-up-process2").delay(400).fadeIn();
  setTimeout(function(){
    $('.email-verification').fadeOut();
  }, 3000);
  $('.otp-verification').delay(3500).fadeIn();
}

function gotosignupprocess3() {
  $(".sign-up-process2").fadeOut();
  $(".sign-up-process3").delay(400).fadeIn();
}

function gotosignupprocess4() {
  $(".sign-up-process3").fadeOut();
  $(".sign-up-process4").delay(400).fadeIn();
}

function gotostudyschedule(){
  $('#tab2').trigger('click');
}

function gotoselectcoursedays(){
  $('.selectcoursedate').fadeOut();
  $('.selectcoursedays').delay(400).fadeIn();
}
function gotostudydays(){
  $('.selectcoursedays').fadeOut();
  $('.selectstudydays').delay(400).fadeIn();
}

function gotonextcourse(){
  $('#tabStudy2').trigger('click');
  $('#tabStudy1').addClass('tab-header-checked');
}

function gotoselectcoursedays2(){
  $('.selectcoursedate2').fadeOut();
  $('.selectcoursedays2').delay(400).fadeIn();
}
function gotostudydays2(){
  $('.selectcoursedays2').fadeOut();
  $('.selectstudydays2').delay(400).fadeIn();
}
