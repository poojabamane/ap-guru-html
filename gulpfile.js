var gulp = require('gulp');
// For css
var cleanCSS = require('gulp-clean-css');
// For adding prefix
var autoprefixer = require('gulp-autoprefixer');
// For js
var uglify = require('gulp-uglify');

var watch = require('gulp-watch');

var series = require("gulp-series");


gulp.task('css', function() {
  return gulp.src('src/css/*.css')
    // to minify css
    .pipe(cleanCSS({compatibility: 'ie8'}))
    // to add prefix
    // .pipe(autoprefixer({
    //     browsers: ['last 2 version', 'safari 5', 'ie 9']
    // }))
    .pipe(gulp.dest('dest/css/'));
});

gulp.task('jsMinify', function() {
    return gulp.src('src/js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('dest/js'));
});

gulp.task('watch', function(){
    gulp.watch('src/css/*', gulp.series('css'));
    gulp.watch('src/js/*', gulp.series('jsMinify'));
});