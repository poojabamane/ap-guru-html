<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include 'head.php';
        ?>
        <title>Journey | AP Guru</title>
        <meta name="description" content="">
    </head>
    <body class="active-page" id="journey-page">
        <?php
            include 'header.php';
            include 'sidebar.php';
        ?>
            <div class="content-wrapper">
                <div class="row ">
                    <div class="col-12 d-flex align-items-center">
                        <a href="#" class="add-back mr-2">
                            <img src="<?= $baseurl; ?>dest/images/icons/back-button.svg" alt="">
                        </a>    
                        <h3>Test 16 <span class="test-sub-details">Sentence Structures | Punctuations</span></h3>
                    </div>
                </div>   
                <div class="row mt-4">
                    <div class="col-2">
                        <div class="card card-score bg-blue topics">
                            <div class="card-header">
                                <h5>Overall Score</h5>
                            </div>
                            <div class="card-body mt-3 mb-3">
                                <div class="progress mx-auto">
                                    <span class="progress-left">
                                        <span class="progress-bar"></span>
                                    </span>
                                    <span class="progress-right">
                                        <span class="progress-bar"></span>
                                    </span>
                                    <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                        <div class="h3">26</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="card icon-card text-center">
                            <img src="<?= $baseurl; ?>dest/images/icons/journey/english.svg" alt="" class="mb-2">
                            <h3 class="card-score text-blue">25</h3>
                            <p class="card-text mb-3">English</p>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="card icon-card text-center">
                            <img src="<?= $baseurl; ?>dest/images/icons/journey/math.svg" alt="" class="mb-2">
                            <h3 class="card-score text-orange">23</h3>
                            <p class="card-text mb-3">Maths</p>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="card icon-card text-center">
                            <img src="<?= $baseurl; ?>dest/images/icons/journey/reading.svg" alt="" class="mb-2">
                            <h3 class="card-score text-green">30</h3>
                            <p class="card-text mb-3">Reading</p>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="card icon-card text-center">
                            <img src="<?= $baseurl; ?>dest/images/icons/journey/science.svg" alt="" class="mb-2">
                            <h3 class="card-score text-red">27</h3>
                            <p class="card-text mb-3">Science</p>
                        </div>
                    </div>
                </div> 
                <div class="row mt-4">
                    <div class="col-6">
                        <div class="card card-box tests">
                            <div class="card-header">
                                <div class="d-flex">
                                    <h5>Performance By Section - Graph</h5>
                                </div>
                            </div>
                            <div class="card-body"></div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card card-box tests">
                            <div class="card-header">
                                <div class="d-flex">
                                    <h5>Performance By Section - Table</h5>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table w-100 journey-section text-center mb-0">
                                        <thead class="text-uppercase">
                                            <tr>
                                                <th><p>Section</p></th>
                                                <th><p>Correct</p></th>
                                                <th><p>incorrect</p></th>
                                                <th><p>omitted</p></th>
                                                <th><p>total</p></th>
                                                <th><p>% correct</p></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><a href="#"><p>English</p></a></td>
                                                <td><p>61</p></td>
                                                <td><p>14</p></td>
                                                <td><p>0</p></td>
                                                <td><p>75</p></td>
                                                <td><p>81%</p></td>
                                            </tr>
                                            <tr>
                                                <td><a href="#"><p>Maths</p></a></td>
                                                <td><p>37</p></td>
                                                <td><p>23</p></td>
                                                <td><p>0</p></td>
                                                <td><p>60</p></td>
                                                <td><p>62%</p></td>
                                            </tr>
                                            <tr>
                                                <td><a href="#"><p>Reading</p></a></td>
                                                <td><p>34</p></td>
                                                <td><p>6</p></td>
                                                <td><p>0</p></td>
                                                <td><p>40</p></td>
                                                <td><p>85%</p></td>
                                            </tr>
                                            <tr>
                                                <td><a href="#"><p>Science</p></a></td>
                                                <td><p>61</p></td>
                                                <td><p>14</p></td>
                                                <td><p>0</p></td>
                                                <td><p>75</p></td>
                                                <td><p>81%</p></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <a href="#" class="journey-details"><p class="NunitoSans-Bold text-blue">View Details ></p></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
            include 'footer.php';
        ?>
    </body>
</html>