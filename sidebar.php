<aside class="position-fixed sidebar scrollbar">
    <nav class="navbar">
        <ul class="navbar-nav w-100">
            <li class="nav-item d-flex dashboard-page nav-link-orange">
                <a href="<?= $baseurl; ?>dashboard/" class="nav-link d-flex align-items-center w-100">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="18" viewBox="0 0 18 18">
                    <defs>
                        <linearGradient id="linear-gradient-home" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                        <stop offset="0" stop-color="#f5d494"/>
                        <stop offset="1" stop-color="#ef9857"/>
                        </linearGradient>
                    </defs>
                    <path id="Icon_color" data-name="Icon color" d="M18,9.24V17a1,1,0,0,1-1,1H13a1,1,0,0,1-1-1V11.5a.5.5,0,0,0-.5-.5h-5a.5.5,0,0,0-.5.5V17a1,1,0,0,1-1,1H1a1,1,0,0,1-1-1V9.24A3,3,0,0,1,.88,7.12L7.71.29A1,1,0,0,1,8.41,0H9.59a1,1,0,0,1,.7.29l6.83,6.83A3,3,0,0,1,18,9.24Z"/>
                    </svg>
                    <p>Home</p>
                </a>
            </li>
            <li class="nav-item d-flex align-items-center topics-page nav-link-blue">
                <a href="<?= $baseurl; ?>topics/" class="nav-link d-flex align-items-center w-100">                    
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="20.001" viewBox="0 0 16 20.001">
                        <defs>
                            <linearGradient id="linear-gradient-topics" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                            <stop offset="0" stop-color="#8e8fee"/>
                            <stop offset="1" stop-color="#6868e4"/>
                            </linearGradient>
                        </defs>
                        <path id="Icon_color" data-name="Icon color" d="M8.383,20H7.717a1,1,0,0,1-.947-.681L6.26,17.78A7.016,7.016,0,0,0,3.577,14.6C1.819,13.207,0,11.764,0,8A8,8,0,0,1,16,8c0,3.756-1.791,5.2-3.524,6.6A6.9,6.9,0,0,0,9.84,17.78L9.33,19.32A1,1,0,0,1,8.383,20ZM8,5a3,3,0,1,0,3,3A3,3,0,0,0,8,5Z" transform="translate(0 0)"/>
                    </svg>
                    <p>Topics</p>
                </a>
            </li>
            <!-- <li class="nav-item d-flex align-items-center">
                <a href="<?= $baseurl; ?>topics/" class="nav-link d-flex align-items-center w-100">
                    <svg>
                        <path d="M16,19H2a2,2,0,0,1-2-2V4A2,2,0,0,1,2,2H3V.5A.5.5,0,0,1,3.5,0h1A.5.5,0,0,1,5,.5V2h8V.5a.5.5,0,0,1,.5-.5h1a.5.5,0,0,1,.5.5V2h1a2,2,0,0,1,2,2V17A2,2,0,0,1,16,19ZM2,6V17H16V6Zm7.5,8h-5a.5.5,0,0,1-.5-.5v-1a.5.5,0,0,1,.5-.5h5a.5.5,0,0,1,.5.5v1A.5.5,0,0,1,9.5,14Zm4-4h-9A.5.5,0,0,1,4,9.5v-1A.5.5,0,0,1,4.5,8h9a.5.5,0,0,1,.5.5v1A.5.5,0,0,1,13.5,10Z" />
                    </svg>
                    <p>Live Classes</p>
                </a>
            </li> -->
            <li class="nav-item d-flex align-items-center nav-link-red">
                <a href="<?= $baseurl; ?>topics/" class="nav-link d-flex align-items-center w-100">           
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="18.998" viewBox="0 0 18 18.998">
                        <defs>
                            <linearGradient id="linear-gradient-classes" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                            <stop offset="0" stop-color="#ffbbb8"/>
                            <stop offset="1" stop-color="#e07a7a"/>
                            </linearGradient>
                        </defs>
                        <path id="Union_9" data-name="Union 9" d="M17200-7717a2,2,0,0,1-2-2v-13a2,2,0,0,1,2-2h1v-1.5a.5.5,0,0,1,.5-.5h1a.5.5,0,0,1,.5.5v1.5h8v-1.5a.5.5,0,0,1,.5-.5h1a.5.5,0,0,1,.5.5v1.5h1a2,2,0,0,1,2,2v13a2,2,0,0,1-2,2Zm0-2h14v-11h-14Zm2.5-3a.5.5,0,0,1-.5-.5v-1a.5.5,0,0,1,.5-.5h5a.5.5,0,0,1,.5.5v1a.5.5,0,0,1-.5.5Zm0-4a.5.5,0,0,1-.5-.5v-1a.5.5,0,0,1,.5-.5h9a.5.5,0,0,1,.5.5v1a.5.5,0,0,1-.5.5Z" transform="translate(-17198.002 7735.999)"/>
                    </svg>

                    <p>Classes</p>
                </a>
            </li>
            <li class="nav-item d-flex align-items-center nav-link-green">
                <a href="<?= $baseurl; ?>topics/" class="nav-link d-flex align-items-center w-100">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20.947" height="20.95" viewBox="0 0 20.947 20.95">
                        <defs>
                            <linearGradient id="linear-gradient-pratical" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                            <stop offset="0" stop-color="#c5e6e0"/>
                            <stop offset="1" stop-color="#7ebfba"/>
                            </linearGradient>
                        </defs>
                        <path id="Union_8" data-name="Union 8" d="M17207.648-7766.059a9.994,9.994,0,0,1-9.641-9.641,10,10,0,0,1,8.936-10.3.933.933,0,0,1,.719.233.932.932,0,0,1,.3.687v8.171a.9.9,0,0,0,.27.644.912.912,0,0,0,.643.268h8.15a.9.9,0,0,1,.678.294.9.9,0,0,1,.238.7,9.992,9.992,0,0,1-9.934,8.942C17207.889-7766.052,17207.768-7766.054,17207.648-7766.059Zm2.795-11.606a.829.829,0,0,1-.828-.83v-6.671a.832.832,0,0,1,.828-.83,7.507,7.507,0,0,1,7.5,7.5.83.83,0,0,1-.83.83Zm.83-1.66h4.939a5.811,5.811,0,0,0-1.646-3.292,5.809,5.809,0,0,0-3.293-1.646Z" transform="translate(-17197.5 7786.502)" stroke="rgba(0,0,0,0)" stroke-miterlimit="10" stroke-width="1"/>
                    </svg>
                    <p>Practice</p>
                </a>
            </li>
            <li class="nav-item d-flex align-items-center tests-page nav-link-orange">
                <a href="<?= $baseurl; ?>tests/" class="nav-link d-flex align-items-center w-100">
                    
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19.998" height="20" viewBox="0 0 19.998 20">
                        <defs>
                            <linearGradient id="linear-gradient-test" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                            <stop offset="0" stop-color="#f5d494"/>
                            <stop offset="1" stop-color="#ef9857"/>
                            </linearGradient>
                        </defs>
                        <path id="Union_7" data-name="Union 7" d="M17212.469-7816.151l-5.941-3.72a1,1,0,0,0-1.059,0l-5.941,3.72a1,1,0,0,1-1.016.026,1,1,0,0,1-.51-.873v-17a2,2,0,0,1,2-2h12a2,2,0,0,1,2,2v17a.993.993,0,0,1-.514.873.986.986,0,0,1-.484.127A1,1,0,0,1,17212.469-7816.151ZM17216-7818v-16h1a1,1,0,0,1,1,1v14a1,1,0,0,1-1,1Z" transform="translate(-17198.002 7835.999)"/>
                    </svg>

                    <p>Tests</p>
                </a>
            </li>
            <li class="nav-item d-flex align-items-center nav-link-blue">
                <a href="<?= $baseurl; ?>journey/" class="nav-link d-flex align-items-center w-100">
                                    
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19.043" height="19.074" viewBox="0 0 19.043 19.074">
                        <defs>
                            <linearGradient id="linear-gradient-journey" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                            <stop offset="0" stop-color="#8e8fee"/>
                            <stop offset="1" stop-color="#6868e4"/>
                            </linearGradient>
                        </defs>
                        <path id="Union_6" data-name="Union 6" d="M17206.816-7867.993l-8.52-5a.488.488,0,0,1-.264-.437.481.481,0,0,1,.264-.432l1.559-.683,6.91,3.759a.5.5,0,0,0,.479,0l6.914-3.759,1.555.683a.47.47,0,0,1,.32.4.487.487,0,0,1-.217.466l-8.52,5a.507.507,0,0,1-.242.062A.492.492,0,0,1,17206.816-7867.993Zm0-4.558-8.52-4.9a.483.483,0,0,1-.26-.428.477.477,0,0,1,.26-.423l1.828-.782,6.641,3.612a.521.521,0,0,0,.479,0l6.641-3.612,1.832.782a.481.481,0,0,1,.313.393.49.49,0,0,1-.213.458l-8.52,4.9a.457.457,0,0,1-.24.068A.457.457,0,0,1,17206.816-7872.551Zm-.051-4.489-8.521-4.632a.446.446,0,0,1-.246-.423.45.45,0,0,1,.3-.389l8.516-3.479a.5.5,0,0,1,.385,0l8.516,3.479a.45.45,0,0,1,.3.389.446.446,0,0,1-.246.423l-8.521,4.632a.5.5,0,0,1-.238.058A.519.519,0,0,1,17206.766-7877.041Z" transform="translate(-17197.496 7886.505)" stroke="rgba(0,0,0,0)" stroke-miterlimit="10" stroke-width="1"/>
                    </svg>

                    <p>Journey</p>
                </a>
            </li>
            <li class="nav-item d-flex align-items-center nav-link-green">
                <a href="<?= $baseurl; ?>" class="nav-link d-flex align-items-center w-100">                                
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19.043" height="19.07" viewBox="0 0 19.043 19.07">
                    <defs>
                        <linearGradient id="linear-gradient-material" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                        <stop offset="0" stop-color="#c5e6e0"/>
                        <stop offset="1" stop-color="#7ebfba"/>
                        </linearGradient>
                    </defs>
                    <path id="Union_24" data-name="Union 24" d="M17206.816-7917.992l-8.52-5a.493.493,0,0,1-.264-.437.493.493,0,0,1,.264-.437l1.559-.678,6.91,3.759a.5.5,0,0,0,.479,0l6.914-3.759,1.555.678a.483.483,0,0,1,.32.406.491.491,0,0,1-.217.467l-8.52,5a.533.533,0,0,1-.242.058A.517.517,0,0,1,17206.816-7917.992Zm0-4.562-8.52-4.9a.476.476,0,0,1-.26-.423.476.476,0,0,1,.26-.423l1.828-.782,6.641,3.612a.521.521,0,0,0,.479,0l6.641-3.612,1.832.782a.475.475,0,0,1,.313.393.483.483,0,0,1-.213.454l-8.52,4.9a.457.457,0,0,1-.24.068A.457.457,0,0,1,17206.816-7922.554Zm-.051-4.489-8.521-4.627a.451.451,0,0,1-.246-.423.451.451,0,0,1,.3-.389l8.516-3.478a.5.5,0,0,1,.385,0l8.516,3.478a.451.451,0,0,1,.3.389.451.451,0,0,1-.246.423l-8.521,4.627a.481.481,0,0,1-.238.062A.5.5,0,0,1,17206.766-7927.043Z" transform="translate(-17197.496 7936.504)" stroke="rgba(0,0,0,0)" stroke-miterlimit="10" stroke-width="1"/>
                    </svg>
                    <p>Course Material</p>
                </a>
            </li>
           
            <li class="nav-item d-flex align-items-center nav-link-red">
                <a href="<?= $baseurl; ?>" class="nav-link d-flex align-items-center w-100">
                    <svg id="browser_windows" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22" height="18" viewBox="0 0 22 18">
                        <defs>
                            <linearGradient id="linear-gradient-flashcard" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                            <stop offset="0" stop-color="#8e8fee"/>
                            <stop offset="1" stop-color="#6868e4"/>
                            </linearGradient>
                        </defs>
                        <path id="Icon_color" data-name="Icon color" d="M18,18H6a2,2,0,0,1-2-2H18a2,2,0,0,0,2-2V4a2,2,0,0,1,2,2v8A4,4,0,0,1,18,18Zm-2-4H2a2,2,0,0,1-2-2V2A2,2,0,0,1,2,0H16a2,2,0,0,1,2,2V12A2,2,0,0,1,16,14ZM2,4v8H16V4Z" transform="translate(0)"/>
                    </svg>
                    <p>Flashcards</p>
                </a>
            </li>
            <li class="nav-item d-flex align-items-center nav-link-blue">
                <a href="<?= $baseurl; ?>" class="nav-link d-flex align-items-center w-100">
                    
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20.998" height="18.998" viewBox="0 0 20.998 18.998">
                    <defs>
                        <linearGradient id="linear-gradient-contact" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                        <stop offset="0" stop-color="#ffbbb8"/>
                        <stop offset="1" stop-color="#e07a7a"/>
                        </linearGradient>
                    </defs>
                    <path id="Union_4" data-name="Union 4" d="M17199-7968a1,1,0,0,1-1-1,7,7,0,0,1,4.316-6.46,7.014,7.014,0,0,1,7.623,1.508,5,5,0,0,1,5.258-.536,5,5,0,0,1,2.8,4.485,1,1,0,0,1-1,1h-5a1,1,0,0,1-1,1Zm11-11a3,3,0,0,1,3-3,3,3,0,0,1,3,3,3,3,0,0,1-3,3A3,3,0,0,1,17210-7979Zm-9-3a4,4,0,0,1,4-4,4,4,0,0,1,4,4,4,4,0,0,1-4,4A4,4,0,0,1,17201-7982Z" transform="translate(-17197.502 7986.499)" stroke="rgba(0,0,0,0)" stroke-miterlimit="10" stroke-width="1"/>
                    </svg>

                    <p>Contact Us</p>
                </a>
            </li>
        </ul>
    </nav>
</aside>