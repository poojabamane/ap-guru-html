<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'head.php';
    ?>
    <title>Courses | AP Guru</title>
    <meta name="description" content="">
</head>

<body class="loginBack">
    <?php
    include 'header.php';
    ?>
    <section class="vh-100 d-flex align-items-center">
        <div class="col-8 offset-2">
            <div class="card">
                <div class="card-header card-header-title">
                    <div class="container">
                        <div class="tab-wrapper">
                            <a href="javascript:;" class="btn-prev"><img src="<?= $baseurl; ?>dest/images/icons/arrow-prev.svg" alt=""></a>
                            <div class="tab-header tab-header-course tab-header-checked active" id="tab1">
                                <span><h5>Select Course</h5></span>
                            </div>
                            <div class="tab-header tab-header-course" id="tab2">
                                <span><h5>Study Schedule</h5></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-body tab1 active">
                        <div class="card-body-head container">
                            <h5 class=" ">Select the Course you want to study for.</h5>
                            <p>You can select more than one.</p>
                        </div>
                        <form>
                            <div class="container">
                                <div class="row ">
                                    <div class="form-group col-md-6">
                                        <label class="checkbox-wrap checkbox-container checked">
                                            <p class="course-title">SAt</p>
                                            <input type="checkbox" id="satCourse" name="selectCourse" checked>
                                            <span class="checkmark"></span>
                                            <div class="check-rate text-right">
                                                <h5>&#8377; 5,000</h5>
                                                <p>till exam dates</p>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="checkbox-wrap checkbox-container">
                                            <p class="course-title">IB Math </p>
                                            <input type="checkbox" id="ibMathCourse" name="selectCourse">
                                            <span class="checkmark"></span>
                                            <div class="check-rate text-right">
                                                <h5>&#8377; 5,000</h5>
                                                <p>till exam dates</p>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="checkbox-wrap checkbox-container">
                                            <p class="course-title">ACT</p>
                                            <input type="checkbox" id="actCourse" name="selectCourse">
                                            <span class="checkmark"></span>
                                            <div class="check-rate text-right">
                                                <h5>&#8377; 2,500</h5>
                                                <p>(3 months access)</p>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="checkbox-wrap checkbox-container">
                                            <p class="course-title">IB Physics</p>
                                            <input type="checkbox" id="ibPhysicsCourse" name="selectCourse">
                                            <span class="checkmark"></span>
                                            <div class="check-rate text-right">
                                                <h5>&#8377; 2,500</h5>
                                                <p>(3 months access)</p>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="checkbox-wrap checkbox-container">
                                            <p class="course-title">Study Abroad Counselling</p>
                                            <input type="checkbox" id="studyAbroadCourse" name="selectCourse">
                                            <span class="checkmark"></span>
                                            <div class="check-rate text-right">
                                                <h5>&#8377; 2,500</h5>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="checkbox-wrap checkbox-container ">
                                            <p class="course-title">AP Calculus</p>
                                            <input type="checkbox" id="apCalculusCourse" name="selectCourse">
                                            <span class="checkmark"></span>
                                            <div class="check-rate text-right">
                                                <h5>&#8377; 1,800</h5>
                                                <p>till exam dates</p>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <p class="text-right text-custom-secondary d-flex justify-content-end">Start with your 14 days free trial.  <a href="javascript:;" class="btn-next d-flex">Next <img src="<?= $baseurl; ?>images/icons/arrow-next.svg" alt=""> </a></p>
                            </div>
                        </form>
                    </div>
                    <div class="tab-body tab2" style="display:none">
                        <div class="container">
                            <div class="tab-wrapper tab-wrapper-study">
                                <div class="tab-header tab-header-study active" id="tabStudy1">
                                    <span><h5>SAT</h5></span>
                                </div>
                                <div class="tab-header tab-header-study" id="tabStudy2">
                                    <span><h5>New</h5></span>
                                </div>

                            </div>
                        </div>

                        <div class="tab-study-body tabStudy1 active">
                            <form>
                                <div class="container">
                                    <div class="row ">
                                        <div class="col-12">
                                            <label class="schedule-title">Exam Date</label> 
                                        </div>
                                        <div class="col-12">
                                            <div class="option-wrapper">
                                                <label class="option-inline">
                                                    <input type="radio" class="option-button" name="examDate" checked id="mar242020" value="Part - Time">
                                                    <label class="option-line-button" for="mar242020">March 14, 2020</label>
                                                </label>
                                                <label class="option-inline">
                                                    <input type="radio" class="option-button" name="examDate" id="may0202020" value="Full - Time">
                                                    <label class="option-line-button" for="may0202020">May 2, 2020</label>
                                                </label>
                                                <label class="option-inline">
                                                    <input type="radio" class="option-button" name="examDate" id="jun062020" value="Work From Home">
                                                    <label class="option-line-button" for="jun062020">June 6, 2020</label>
                                                </label>
                                                <label class="option-inline">
                                                    <input type="radio" class="option-button" name="examDate" id="jul082020" value="Work From Home">
                                                    <label class="option-line-button" for="jul082020">July 8, 2020</label>
                                                </label>
                                                <label class="option-inline">
                                                    <input type="radio" class="option-button" name="examDate" id="aug042020" value="Work From Home">
                                                    <label class="option-line-button" for="aug042020">Aug 4, 2020</label>
                                                </label>
                                                
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label class="schedule-title">Select the days of study</label> 
                                        </div>
                                        <div class="col-8">
                                            <div class="option-wrapper">
                                                <label class="option-inline">
                                                    <input type="checkbox" class="option-button" name="select-days" checked id="monday" value="Part - Time">
                                                    <label class="option-flat-button" for="monday">M</label>
                                                </label>
                                                <label class="option-inline">
                                                    <input type="checkbox" class="option-button" name="select-days" id="tuesday" value="Full - Time">
                                                    <label class="option-flat-button" for="tuesday">T</label>
                                                </label>
                                                <label class="option-inline">
                                                    <input type="checkbox" class="option-button" name="select-days" id="wednesday" value="Work From Home">
                                                    <label class="option-flat-button" for="wednesday">W</label>
                                                </label>
                                                <label class="option-inline">
                                                    <input type="checkbox" class="option-button" name="select-days" id="thursday" value="Work From Home">
                                                    <label class="option-flat-button" for="thursday">T</label>
                                                </label>
                                                <label class="option-inline">
                                                    <input type="checkbox" class="option-button" name="select-days" id="friday" value="Work From Home">
                                                    <label class="option-flat-button" for="friday">F</label>
                                                </label>
                                                <label class="option-inline">
                                                    <input type="checkbox" class="option-button" name="select-days" id="saturday" value="Work From Home">
                                                    <label class="option-flat-button" for="saturday">S</label>
                                                </label>
                                                <label class="option-inline">
                                                    <input type="checkbox" class="option-button" name="select-days" id="sunday" value="Work From Home">
                                                    <label class="option-flat-button" for="sunday">S</label>
                                                </label>
                                                
                                            </div>
                                        </div>
                                        <div class="col-4">
                                           
                                        </div>
                                    </div>
                                    <p class="text-right text-custom-secondary d-flex justify-content-end">Start with your 14 days free trial.  <a href="javascript:;" class="btn-next d-flex">Next <img src="<?= $baseurl; ?>images/icons/arrow-next.svg" alt=""> </a></p>
                                </div>
                            </form>
                        </div>
                        <div class="tab-study-body tabStudy2"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    include 'footer.php';
    ?>
    <script src="<?= $baseurl; ?>dest/js/matchHeight.js"></script>
    <script>
        $(".checkbox-container").matchHeight();
        $(document).ready(function(){
            $('.tab-header-course').click(function(){
                var tab_id=this.id;
                if($(this).hasClass('active')){

                }
                else
                {
                    $('.active').parent().find('.tab-body').fadeOut();
                    $('.tab-header-course').removeClass('active');
                    $(".checkbox-container").matchHeight();
                }
                $('.' + tab_id).delay(400).fadeToggle("slow");
                $(this).toggleClass('active');
                
            });
            $('.tab-header-study').click(function(){
                var tab_id=this.id;
                if($(this).hasClass('active')){

                }
                else
                {
                    $('.active').parent().find('.tab-study-body').fadeOut();
                    $('.tab-header-study').removeClass('active');                   
                    $(".checkbox-container").matchHeight();
                }
                $('.' + tab_id).delay(400).fadeToggle("slow");
                $(this).toggleClass('active');
                
            });
        });
    </script>
</body>

</html>