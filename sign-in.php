<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'head.php';
    ?>
    <title>Sign In | AP Guru</title>
    <meta name="description" content="">
</head>

<body class="noheader">
    <?php
    include 'header.php';
    ?>
    <section class="vh-100 d-flex align-items-center">
        <div class="col-4 offset-4">
            <div class="card">
                <div class="card-header text-center">
                    <img src="<?= $baseurl; ?>dest/images/logo.svg" alt="">
                </div>
                <div class="card-body">
                    <h4 class="text-center">Login to Account</h4>
                    <form>
                        <div class="form-group">
                            <label for="signin-number">Registered Email id or Mobile Number</label>
                            <input type="text" class="form-control" id="signin-number">
                        </div>
                        <div class="form-group">
                            <label for="signin-password">Password</label>
                            <input type="password" class="form-control" id="signin-password">
                            <div class="forgot-password text-right"><a>Forget Password?</a></div>
                        </div>
                        <div class="form-group">
                            <label class="checkbox-wrap">Remember Password
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-gradient">Sign In</button>
                    </form>
                </div>
                <div class="card-footer text-center">
                    <p>Don’t have an account? <a href="<?= $baseurl; ?>sign-up/" class="cust-link">Create Account</a></p>
                </div>
            </div>
        </div>
    </section>
    <?php
    include 'footer.php';
    ?>
</body>

</html>